Siddhinath kharade

This is simple assignment.
 Date:  19:07:2018
 Version:1
 What:  WordCount Program written in scala and run using spark cluster single cluster or mulple node cluster support.

 Input:     takes input file from command line
 Output:    Stored the key,valae pairs into a output file that provided by command line.

 Fetch and store both operation done this program form HDFS.

 How to run.
    Need maven support to be run.

    Requirements????
    1) Single node hadoop cofigured cluster.. running process with atleast one datanode present
    2) Spark cluster installed. using hadoop dfs for processing data.
    3) Maven latest version
        (If progrma does not support installed version then please check "pom.xml" any further details to run.)
        Mine installed versions
            hadoop 2.9.0
            spark  2.3.1
            scala  2.11.6
            maven  2.3._

            If any changes in version please update pom.xml file contain in directory.

    Steps ro run.console should present in project name directory
    1)maven compile
    2)maven package
    3)spark-submit --class App --master local[8] target/Example-1.jar "input file name/path" "output file name path"

    File should be contain in hdfs.

    Thank You.
